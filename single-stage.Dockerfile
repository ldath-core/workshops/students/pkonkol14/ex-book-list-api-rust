FROM rust:1.67

WORKDIR /var/app
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
COPY migrations /var/app/migrations
RUN cargo build && cp ./target/debug/ex-book-list-api-rust /bin/ex-book-list-api-rust

EXPOSE 8080

ENTRYPOINT ["/bin/ex-book-list-api-rust"]
CMD ["-vvv", "serve", "--bind", "0.0.0.0", "--port", "8080", "--migrate", "--load"]
