DO $$ BEGIN
    CREATE TYPE public.image_t AS (url text, base64 text);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;

CREATE TABLE IF NOT EXISTS public.books (
    id integer NOT NULL,
    title text NOT NULL,
    author text NOT NULL,
    year text NOT NULL,
    description text,
    image image_t
);

CREATE SEQUENCE IF NOT EXISTS public.books_id_seq AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.books_id_seq OWNED BY public.books.id;

ALTER TABLE ONLY public.books
    ALTER COLUMN id
    SET DEFAULT nextval('public.books_id_seq'::regclass);
