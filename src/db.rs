mod models;

#[cfg(test)]
mod tests;

use anyhow::Result;
use async_trait::async_trait;
use sqlx::{
    migrate::Migrator,
    prelude::*,
    {query, query_as, PgPool},
};
use std::path::Path;
use tracing::{debug, error, info, instrument};

#[cfg(test)]
use mockall::predicate::*;

use crate::{
    router::NewBook,
    traits::{BooksRepo, DatabaseConnector, DatabaseHandler},
};
pub use models::{Book, Image};

const MIGRATIONS_PATH: &str = "./migrations";

/// Contains DB connection and implements layer for interacting with books database
#[derive(Debug, Clone)]
pub struct Db {
    pool: PgPool,
}

impl Db {
    #[instrument]
    pub async fn new(pool: PgPool) -> Result<Self> {
        Ok(Self { pool })
    }

    #[instrument(skip(conn_str))]
    pub async fn connect_pool(conn_str: &str) -> Result<PgPool> {
        debug!("postgres connection string: {}", conn_str);
        let pool = sqlx::PgPool::connect(conn_str).await?;
        info!("Connected to postgres");
        Ok(pool)
    }

    /// Drops table books and related
    #[instrument]
    async fn drop_all_tables(&self) -> Result<()> {
        let _ = query("DROP TYPE image_t CASCADE").execute(&self.pool).await;
        let _ = query("DROP TABLE books_id_seq").execute(&self.pool).await;
        let _ = query("DROP TABLE _sqlx_migrations")
            .execute(&self.pool)
            .await;
        let q = query("DROP TABLE books").execute(&self.pool).await;
        if let Err(e) = q {
            error!("couldn't drop table err: {}", e);
        }
        debug!("Dropped books tables");
        Ok(())
    }
}

impl DatabaseHandler for Db {}

/// Abstract away DB internals exposing interface for interacting with books data
#[async_trait]
impl BooksRepo for Db {
    #[instrument]
    async fn count_books(&self) -> Result<i64> {
        let q = query("SELECT COUNT(*) FROM books")
            .fetch_one(&self.pool)
            .await?;
        let count: i64 = q.get("count");
        debug!("count of books: {:?}", count);
        Ok(count)
    }

    #[instrument]
    async fn get_books(&self, skip: i32, limit: i32) -> Result<Vec<Book>> {
        let q = query_as::<_, Book>(
            r#"SELECT id, title, author, year, description, image
            FROM books
            LIMIT $1
            OFFSET $2"#,
        )
        .bind(limit)
        .bind(skip)
        .fetch_all(&self.pool)
        .await?;
        debug!("all books: {:?}", q);
        Ok(q)
    }

    #[instrument]
    async fn get_book(&self, id: i32) -> Result<Book> {
        let q = query_as::<_, Book>(
            r#"SELECT id, title, author, year, description, image
            FROM books
            WHERE id = $1"#,
        )
        .bind(id)
        .fetch_one(&self.pool)
        .await?;
        debug!("book with id {}: {:?}", id, q);
        Ok(q)
    }

    #[instrument]
    async fn create_book(&self, b: NewBook) -> Result<Book> {
        debug!("creating book {:?}", b);
        let q = query_as::<_, Book>(
            r#"INSERT INTO books(title, author, year, description, image)
            VALUES ($1, $2, $3, $4, $5)
            RETURNING *"#,
        )
        .bind(b.title)
        .bind(b.author)
        .bind(b.year)
        .bind(b.description)
        .bind(b.image.map(Image::from))
        .fetch_one(&self.pool)
        .await?;
        Ok(q)
    }

    #[instrument]
    async fn delete_book(&self, id: i32) -> Result<u64> {
        debug!("deleting book {} from DB", id);
        let q = query(
            r#"DELETE FROM books
            WHERE id = $1"#,
        )
        .bind(id)
        .execute(&self.pool)
        .await?;
        let rows = q.rows_affected();
        debug!("Deleted {} rows from the DB", rows);
        Ok(rows)
    }

    #[instrument]
    async fn update_book(&self, id: i32, b: NewBook) -> Result<Book> {
        debug!("updating book {:?} into db, new ID: {}", b, id);
        let q = query_as::<_, Book>(
            r#"UPDATE books
            SET title = $1, author = $2, year = $3, description = $4, image = $5
            WHERE id = $6
            RETURNING *"#,
        )
        .bind(b.title)
        .bind(b.author)
        .bind(b.year)
        .bind(b.description)
        .bind(b.image.map(Image::from))
        .bind(id)
        .fetch_one(&self.pool)
        .await?;
        Ok(q)
    }
}

#[async_trait]
impl DatabaseConnector for Db {
    /// Deletes all currently existing data from books then inserts test data into
    /// the table
    #[instrument]
    async fn create_test_data(&self) -> Result<()> {
        let mut tx = self.pool.begin().await?;
        let q = query("DELETE FROM books;").execute(&mut tx).await?;
        debug!("Deleted {} entries from books", q.rows_affected());

        let rows = vec![
            ("Golang is great", "Mr. Great", "2012", None, None),
            ("C++ is greatest", "Mr. C++", "2015", Some("desc"), None),
            (
                "C++ is very old",
                "Mr. Old",
                "2014",
                Some("true"),
                Some(Image {
                    url: Some("wp.pl/some.jpg".to_string()),
                    base64: Some("==test".to_string()),
                }),
            ),
        ];
        for row in rows {
            query(
                r#"
                INSERT INTO books(title, author, year, description, image)
                VALUES ($1, $2, $3, $4, $5)
                RETURNING *;
                "#,
            )
            .bind(row.0)
            .bind(row.1)
            .bind(row.2)
            .bind(row.3)
            .bind(row.4)
            .execute(&mut tx)
            .await?;
        }
        tx.commit().await?;
        info!("Cleared books then insterted sample data");
        Ok(())
    }

    /// Sets up necessary underlaying database schema and tables
    #[instrument]
    async fn migrate_db(&self) -> Result<()> {
        self.drop_all_tables().await?;
        let mut m = Migrator::new(Path::new(MIGRATIONS_PATH))
            .await
            .expect("Couldn't initialize the migrator");
        m.set_ignore_missing(true);
        m.run(&self.pool).await?;
        debug!("Database migrated");
        Ok(())
    }

    /// Closes DB connection, makes self unusable
    #[instrument]
    async fn close(&self) {
        self.pool.close().await;
    }

    /// Checks if DB is alive
    #[instrument]
    async fn ping(&self) -> bool {
        let check = self.pool.execute("SELECT 1").await;
        if let Err(e) = check {
            error!("couldn't ping the database, got error: {}", e);
            return false;
        }
        true
    }
}
