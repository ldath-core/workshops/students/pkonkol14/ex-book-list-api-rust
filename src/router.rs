mod handlers;
mod macros;
mod models;

#[cfg(test)]
mod tests;

use axum::{routing::get, Router};
use tower_http::{
    cors::{Any, CorsLayer},
    trace::{DefaultOnRequest, DefaultOnResponse, TraceLayer},
};
use tracing::{info, Level};

use crate::traits::DatabaseHandler;
use handlers::{create_book, delete_book, get_book, get_books, get_health, not_found, update_book};

pub use models::{NewBook, NewImage};

/// Return router configured for the book API
pub fn get_router<D>(db: D) -> Router
where
    D: DatabaseHandler + Clone + Send + Sync + 'static,
{
    let router: Router = Router::new()
        .route("/health", get(get_health::<D>))
        .route("/books", get(get_books::<D>).post(create_book::<D>))
        .route(
            "/books/:id",
            get(get_book::<D>)
                .put(update_book::<D>)
                .patch(update_book::<D>)
                .delete(delete_book::<D>),
        )
        .layer(
            TraceLayer::new_for_http()
                .on_request(DefaultOnRequest::new().level(Level::INFO))
                .on_response(DefaultOnResponse::new().level(Level::INFO)),
        )
        .with_state(db); // Somehow moving state to the front changes the type to Router<Db> and causes problems
    Router::new().fallback(not_found).nest("/v1", router)
}

/// Add CORS layer to ther router
///
/// # Examples
///
/// ```ignore
///  # let router = get_router(db::Db})
///  router = add_cors_layer(router.clone());
/// ```
pub fn add_cors_layer(r: Router) -> Router {
    r.layer(
        CorsLayer::new()
            .allow_origin(Any)
            .allow_headers(Any)
            .allow_credentials(false), // complains when set to true together with Any on headers
    )
}

pub async fn shutdown_signal() {
    tokio::signal::ctrl_c()
        .await
        .expect("Expect shutdown signal handler");
    info!("received shutdown signal");
}
