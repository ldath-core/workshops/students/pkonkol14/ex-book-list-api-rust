use axum::body::HttpBody;
use http::Uri;
use hyper::{Body, Response};
use serde_json::Value;
use std::{
    net::{SocketAddr, TcpListener},
    thread,
};
use testcontainers::{clients::Cli, images::postgres::Postgres, Container};
use tokio::{runtime::Runtime, sync::oneshot};

use crate::{db::Db, router::get_router, traits::DatabaseConnector};

pub fn start_postgres(client: &Cli) -> (Container<Postgres>, String) {
    let container = client.run(Postgres::default());
    let db_str = format!(
        "postgres://postgres:postgres@{}:{}/postgres",
        std::env::var("TESTCONTAINERS_HOST").unwrap_or("localhost".to_string()),
        container.get_host_port_ipv4(5432),
    );
    (container, db_str)
}

/// Retries the request continously for n seconds
pub async fn retry_request(uri: Uri, n: u32) -> Option<Response<Body>> {
    for _ in 0..n * 2 {
        let response = hyper::Client::new().get(uri.clone()).await;
        if let Ok(resp) = response {
            return Some(resp);
        }
        tokio::time::sleep(std::time::Duration::from_millis(500)).await;
    }
    None
}

/// Starts the server in separate thread and waits until it makes a simplest response.
/// The thread is automatically terminated when returned sender goes out of scope.
pub async fn start_background_server(listener: TcpListener, db_str: &str) -> oneshot::Sender<()> {
    let addr = listener.local_addr().unwrap();
    let pool = Db::connect_pool(db_str).await.unwrap();
    let db = Db::new(pool)
        .await
        .expect("Couldn't connect to testcontainers database");
    db.migrate_db().await.unwrap();
    let router = get_router(db);
    // Starting our app in a background thread, rx automatically closes it when we go out of scope
    let (tx, rx) = oneshot::channel::<()>();
    thread::spawn(move || {
        Runtime::new().unwrap().block_on(async move {
            let server = axum::Server::from_tcp(listener)
                .unwrap()
                .serve(router.into_make_service());
            tokio::select! {
                _ = rx => {},
                _ = server => {},
            }
        });
    });
    // wait for the server to start responding
    retry_request(uri(addr, "/v1/health"), 5).await.unwrap();

    tx
}

pub async fn body_as_string<T>(r: hyper::Response<T>) -> String
where
    T: HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();
    let body = String::from_utf8(hyper::body::to_bytes(into).await.unwrap().to_vec()).unwrap();
    body
}

pub async fn body_as_json<T>(r: hyper::Response<T>) -> Value
where
    T: HttpBody,
    T::Error: std::fmt::Debug,
{
    let into = r.into_body();
    let body: Value = serde_json::from_slice(&hyper::body::to_bytes(into).await.unwrap()).unwrap();
    body
}

pub fn uri(addr: SocketAddr, path_and_query: &str) -> Uri {
    let mut parts = http::uri::Parts::default();
    parts.scheme = Some("http".parse().unwrap());
    parts.authority = Some(addr.to_string().parse().unwrap());
    parts.path_and_query = Some(path_and_query.parse().unwrap());
    Uri::from_parts(parts).unwrap()
}

/// Starts a listener on a random port
/// Returns listener instance and corresponding socket address
pub fn get_random_port_listener(ip: &str) -> (TcpListener, SocketAddr) {
    let listener = TcpListener::bind((ip, 0)).unwrap();
    let addr = listener.local_addr().unwrap();
    (listener, addr)
}
