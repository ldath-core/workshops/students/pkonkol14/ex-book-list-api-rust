//! Contains handlers used by the book server

// It is possible to make the code more concise by implementing impl IntoResponse for ApiError
// instead of unwrap_or_return!()
// https://www.propelauth.com/post/clean-code-with-rust-and-axum

use super::{
    macros::{response, unwrap_or_return},
    models::{ApiResponse, NewBook, PaginatedContent, Pagination},
};
use crate::traits::DatabaseHandler;

use axum::{
    extract::{rejection::JsonRejection, Json, Path, Query, State},
    http::StatusCode,
    response::IntoResponse,
};
use serde_json::json;
use tracing::{debug, error, info};

pub(super) async fn not_found() -> impl IntoResponse {
    (
        StatusCode::NOT_FOUND,
        Json(ApiResponse {
            status: StatusCode::NOT_FOUND.as_u16(),
            message: "Page Not Found".to_owned(),
            content: None,
        }),
    )
}

pub(super) async fn get_health<D: DatabaseHandler>(State(db): State<D>) -> impl IntoResponse {
    response!(
        StatusCode::OK,
        "ex-book-list-api-rust api health",
        Some(json!({"alive": true, "postgres": db.ping().await }))
    )
}

pub(super) async fn get_books<D: DatabaseHandler>(
    State(db): State<D>,
    pagination: Option<Query<Pagination>>,
) -> impl IntoResponse {
    let (skip, limit) = match pagination {
        Some(p) => (p.skip, p.limit),
        None => (0, 10),
    };
    let b = unwrap_or_return!(
        db.get_books(skip, limit).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with getting books",
            None
        )
    );
    let count = unwrap_or_return!(
        db.count_books().await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with counting books",
            None
        )
    );
    response!(
        StatusCode::OK,
        format!("books - skip: {skip}; limit: {limit}"),
        Some(
            serde_json::to_value(PaginatedContent {
                count,
                skip,
                limit,
                results: serde_json::to_value(b).unwrap(),
            })
            .unwrap()
        )
    )
}

pub(super) async fn create_book<D: DatabaseHandler>(
    State(db): State<D>,
    Json(b): Json<NewBook>,
) -> impl IntoResponse {
    info!("create_book request for {:?}", b);
    if b.title.is_empty() || b.author.is_empty() || b.year.is_empty() {
        return response!(StatusCode::NOT_ACCEPTABLE, "Fields cannot be empty", None);
    }
    let b = unwrap_or_return!(
        db.create_book(b).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with creating book",
            None
        )
    );
    let j = serde_json::to_value(b).expect("serde_json couldn't hande Book struct");
    response!(StatusCode::CREATED, "ok", Some(j))
}

pub(super) async fn get_book<D: DatabaseHandler>(
    State(db): State<D>,
    Path(id): Path<i32>,
) -> impl IntoResponse {
    debug!("get_book request for id: {}", id);
    let b = unwrap_or_return!(
        db.get_book(id).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with getting book",
            None
        )
    );
    let j = serde_json::to_value(b).expect("serde_json couldn't hande Book struct");
    response!(StatusCode::OK, "book", Some(j))
}

pub(super) async fn update_book<D: DatabaseHandler>(
    State(db): State<D>,
    Path(id): Path<i32>,
    b: Result<Json<NewBook>, JsonRejection>,
) -> impl IntoResponse {
    debug!("update_book request for id: {} with new data {:?}", id, b);
    let b = match b {
        Ok(Json(v)) => v,
        Err(e) => {
            return response!(StatusCode::UNSUPPORTED_MEDIA_TYPE, format!("{:?}", e), None);
        },
    };
    if b.title.is_empty() || b.author.is_empty() || b.year.is_empty() {
        return response!(StatusCode::NOT_ACCEPTABLE, "Fields cannot be empty", None);
    }
    let r = unwrap_or_return!(
        db.update_book(id, b).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with updating book",
            None
        )
    );
    response!(
        StatusCode::ACCEPTED,
        format!("book: {id} updated"),
        Some(serde_json::to_value(r).unwrap())
    )
}

pub(super) async fn delete_book<D: DatabaseHandler>(
    State(db): State<D>,
    Path(id): Path<i32>,
) -> impl IntoResponse {
    debug!("delete_book request for id: {}", id);
    let b = unwrap_or_return!(
        db.delete_book(id).await,
        response!(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Problem with deleting book",
            None
        )
    );
    match b {
        0 => response!(StatusCode::NOT_FOUND, "book not found", None),
        _ => response!(StatusCode::ACCEPTED, format!("book: {b} deleted"), None),
    }
}
