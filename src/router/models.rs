use serde::{Deserialize, Serialize};
use serde_json::Value;

/// Template which is converted into json and sent as a response to every
/// request
#[derive(Serialize)]
pub(super) struct ApiResponse {
    pub(super) status: u16,
    pub(super) message: String,
    pub(super) content: Option<Value>,
}

/// Inserted into [`ApiResponse::content`] as a response to requests that require
/// paginated content
#[derive(Serialize)]
pub(super) struct PaginatedContent {
    pub(super) count: i64,
    pub(super) skip: i32,
    pub(super) limit: i32,
    pub(super) results: Value,
}

/// Query template for handlers that support paginated content like
/// [`super::handlers::get_books`]
#[derive(Deserialize)]
pub(super) struct Pagination {
    pub(super) skip: i32,
    pub(super) limit: i32,
}

/// Query template for handlers that create or update a book and need
/// to have it passed as a parameter like [`super::handlers::create_book`]
#[cfg_attr(test, derive(Clone))]
#[derive(Deserialize, Debug)]
pub struct NewBook {
    pub title: String,
    pub author: String,
    pub year: String,
    pub description: Option<String>,
    pub image: Option<NewImage>,
}

/// Image model embedded inside [`NewBook`]
#[cfg_attr(test, derive(Clone))]
#[derive(Deserialize, Debug)]
pub struct NewImage {
    pub url: Option<String>,
    pub base64: Option<String>,
}
