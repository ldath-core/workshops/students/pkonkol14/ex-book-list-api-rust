use super::*;
use crate::{router::NewImage, tests::utils::start_postgres};

use once_cell::sync::Lazy;
use testcontainers::{clients::Cli, images::postgres::Postgres, Container};

#[allow(clippy::redundant_closure)]
static CLIENT: Lazy<Cli> = Lazy::new(|| Cli::default());

#[tokio::test]
async fn test_get_books() {
    let (_container, db) = prepare_test().await;
    db.create_test_data().await.unwrap();

    let result = db.get_books(0, 10).await;
    assert!(matches!(result, Ok(_)));
}

#[tokio::test]
async fn test_create_then_get_book() {
    let tested_book = new_book;

    let (_container, db) = prepare_test().await;

    let created = db.create_book(tested_book()).await.unwrap();

    let queried = db.get_book(created.id).await.unwrap();
    assert_eq!(created, queried);
}

#[tokio::test]
async fn test_create_then_remove_book() {
    let tested_book = new_book;

    let (_container, db) = prepare_test().await;

    let created = db.create_book(tested_book()).await.unwrap();

    let deleted = db.delete_book(created.id).await.unwrap();
    assert_eq!(deleted, 1);

    let queried = db.get_book(created.id).await;
    assert!(matches!(queried, Err(_)));
}

#[tokio::test]
async fn test_create_then_update_book() {
    let mut tested_book = new_book();

    let (_container, db) = prepare_test().await;

    let created = db.create_book(tested_book.clone()).await.unwrap();

    tested_book.title = "newtitle".to_string();
    db.update_book(created.id, tested_book.clone())
        .await
        .unwrap();

    let queried = db.get_book(created.id).await.unwrap();
    assert_eq!(queried, tested_book);
}

#[tokio::test]
async fn test_create_then_retrieve_many() {
    let tested_books = new_books;

    let (_container, db) = prepare_test().await;

    for b in tested_books().into_iter() {
        db.create_book(b).await.unwrap();
    }

    let queried = db.get_books(0, tested_books().len() as i32).await.unwrap();
    for (returned, expected) in queried.iter().zip(tested_books().iter()) {
        assert_eq!(returned, expected);
    }
}

fn new_book() -> NewBook {
    NewBook {
        title: "title".to_string(),
        author: "author".to_string(),
        year: "1234".to_string(),
        description: Some("description".to_string()),
        image: Some(NewImage {
            url: Some("img1.jpg".to_string()),
            base64: Some("=qwasdfzxcv2134".to_string()),
        }),
    }
}

fn new_books() -> Vec<NewBook> {
    vec![
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: Some("description".to_string()),
            image: Some(NewImage {
                url: Some("img1.jpg".to_string()),
                base64: Some("=qwasdfzxcv2134".to_string()),
            }),
        },
        NewBook {
            title: "title2".to_string(),
            author: "author2".to_string(),
            year: "1235".to_string(),
            description: None,
            image: None,
        },
        NewBook {
            title: "title3".to_string(),
            author: "author3".to_string(),
            year: "1236".to_string(),
            description: None,
            image: None,
        },
        NewBook {
            title: "title4".to_string(),
            author: "author4".to_string(),
            year: "1237".to_string(),
            description: Some("desc".to_string()),
            image: None,
        },
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: None,
            image: Some(NewImage {
                url: Some("img1.jpg".to_string()),
                base64: None,
            }),
        },
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: None,
            image: Some(NewImage {
                url: None,
                base64: Some("=qwasdfzxcv2134".to_string()),
            }),
        },
        NewBook {
            title: "title".to_string(),
            author: "author".to_string(),
            year: "1234".to_string(),
            description: None,
            image: Some(NewImage {
                url: None,
                base64: None,
            }),
        },
    ]
}

async fn prepare_test() -> (Container<'static, Postgres>, Db) {
    let (container, db_str) = start_postgres(&CLIENT);
    let db = Db::new(Db::connect_pool(&db_str).await.unwrap())
        .await
        .expect("Couldn't connect to testcontainers database");
    db.migrate_db().await.unwrap();
    (container, db)
}
