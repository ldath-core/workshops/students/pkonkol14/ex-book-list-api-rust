use serde::Serialize;
use sqlx::{FromRow, Type};

#[cfg(test)]
use crate::router::NewBook;
use crate::router::NewImage;

/// Object representing book in a way serializable to and from database
#[cfg_attr(test, derive(Clone, PartialEq))]
#[derive(Serialize, FromRow, Debug)]
pub struct Book {
    pub id: i32,
    pub title: String,
    pub author: String,
    pub year: String,
    pub description: Option<String>,
    pub image: Option<Image>,
}

/// Represents optional image data which is a model of postgres type
#[cfg_attr(test, derive(Clone, PartialEq))]
#[derive(Serialize, FromRow, Debug, Type)]
#[sqlx(type_name = "image_t")]
pub struct Image {
    pub url: Option<String>,
    pub base64: Option<String>,
}

impl From<NewImage> for Image {
    fn from(item: NewImage) -> Self {
        Image {
            url: item.url,
            base64: item.base64,
        }
    }
}

#[cfg(test)]
impl PartialEq<NewBook> for Book {
    fn eq(&self, other: &NewBook) -> bool {
        let image = other.image.as_ref().map(|i| Image::from(i.clone()));
        self.title == other.title
            && self.author == other.author
            && self.year == other.year
            && self.description == other.description
            && self.image == image
    }
}

#[cfg(test)]
impl PartialEq<NewImage> for Image {
    fn eq(&self, other: &NewImage) -> bool {
        self.url == other.url && self.base64 == other.base64
    }
}
