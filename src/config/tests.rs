use super::*;

use assert_fs::fixture::NamedTempFile;
use assert_fs::prelude::*;

#[test]
fn test_load_config() {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(
            r#"---
env: dev
server:
    data:
postgres:
    url: postgres://ex-book-list-api-rust:password123@localhost:5432/ex-book-list-api-rust
logger:
    level: debug
"#,
        )
        .unwrap();
    let c = load_config(tmp_cfg.path());
    assert!(c.env == "dev");
    assert!(c.server.data.is_empty());
    assert!(
        c.postgres.url
            == "postgres://ex-book-list-api-rust:password123@localhost:5432/ex-book-list-api-rust"
    );
    assert!(c.logger.level == "debug");
}
