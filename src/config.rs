#[cfg(test)]
mod tests;

use serde::{Deserialize, Serialize};
use serde_yaml::{self};
use std::path::Path;
use tracing::{debug, instrument};

/// Reads yaml file and creates Config struct for App to use
///
/// Example config file:
/// ```yaml
/// ---
/// env: dev
/// server:
///   data:
/// postgres:
///   url: postgres://user:password@localhost:5432/db-name
/// logger:
///   level: debug
/// ```
#[instrument]
pub fn load_config(path: &Path) -> Config {
    let f = std::fs::File::open(path)
        .unwrap_or_else(|_| panic!("could not open config source file {}", path.display()));
    let config: Config = serde_yaml::from_reader(f).expect("could not parse config at.");
    debug!("Config {} loaded", path.display());
    config
}

/// Config struct for App with all its subelements
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    pub env: String,
    pub server: ServerConfig,
    pub postgres: PostgresConfig,
    pub logger: LoggerConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ServerConfig {
    pub data: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PostgresConfig {
    pub url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LoggerConfig {
    pub level: String,
}
