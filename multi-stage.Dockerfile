FROM rust:1.67 as builder

WORKDIR /var/app
COPY src /var/app/src
COPY Cargo.lock Cargo.toml /var/app/
RUN cargo build && ls target/debug

FROM debian:buster-slim

WORKDIR /var/app
COPY --from=builder /var/app/target/debug/ex-book-list-api-rust /bin/ex-book-list-api-rust
COPY migrations /var/app/migrations
RUN chmod +x /bin/ex-book-list-api-rust
RUN /bin/ex-book-list-api-rust --help

EXPOSE 8080
ENTRYPOINT ["/bin/ex-book-list-api-rust"]
CMD ["-vvv", "serve", "--bind", "0.0.0.0", "--port", "8080", "--migrate", "--load"]
