use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::io::BufReader;
use std::process::{Command, Stdio};
use testcontainers::clients::Cli;
use utils::get_tmp_cfg;

mod utils;
use utils::*;

#[test]
fn help_command_succeeds() {
    let mut cmd = Command::cargo_bin("ex-book-list-api-rust").unwrap();
    cmd.args(["serve", "--help"]);
    cmd.assert().success();
}

#[test]
fn nonexistent_subcommand_fails() {
    let mut cmd = Command::cargo_bin("ex-book-list-api-rust").unwrap();
    cmd.args(["this-subcommand-does-not-exist"]);
    cmd.assert().failure();
}

#[test]
fn default_log_level_from_config() {
    let cfg = get_tmp_cfg("cause-fail", "debug");
    let mut cmd = Command::cargo_bin("ex-book-list-api-rust").unwrap();
    cmd.args(["--config", cfg.path().to_str().unwrap(), "serve"]);

    cmd.assert().stdout(
        predicate::str::contains(r#""verbosity":"Level(Debug)""#)
            .and(predicate::str::contains(r#""format":"Json""#)),
    );
}

#[test]
fn overwrite_log_level_from_cli() {
    let cfg = get_tmp_cfg("cause-fail", "debug");
    let mut cmd = Command::cargo_bin("ex-book-list-api-rust").unwrap();
    cmd.args(["--config", cfg.path().to_str().unwrap(), "-vv", "serve"]);

    cmd.assert()
        .stdout(predicate::str::contains(r#""verbosity":"Level(Trace)""#));
}

#[test]
fn server_starts_and_shutdowns_gracefuly() {
    let client = Cli::default();
    let (_container, db_str) = start_postgres(&client);
    let cfg = get_tmp_cfg(&db_str, "debug");

    let mut cmd = Command::cargo_bin("ex-book-list-api-rust").unwrap();
    cmd.args(["--config", cfg.path().to_str().unwrap(), "serve"]);
    let mut running = cmd.stdout(Stdio::piped()).spawn().unwrap();
    let mut bufread = BufReader::new(running.stdout.take().unwrap());

    read_stdout_until_contains(&mut bufread, "Serve params", 15).unwrap();
    send_sigint(running.id());
    read_stdout_until_contains(&mut bufread, "starting graceful shutdown", 15).unwrap();
}
