use assert_fs::{prelude::*, NamedTempFile};
use std::{
    io::{BufRead, BufReader},
    process::{ChildStdout, Command},
    time::{Duration, Instant},
};

#[must_use]
pub fn get_tmp_cfg(db_str: &str, log_level: &str) -> NamedTempFile {
    let tmp_cfg = NamedTempFile::new("tmp_config.yaml").unwrap();
    tmp_cfg
        .write_str(&format!(
            r#"---
env: dev
server:
  data:
postgres:
  url: {db_str}
logger:
  level: {log_level}
"#
        ))
        .unwrap();
    tmp_cfg
}

use testcontainers::{clients::Cli, images::postgres::Postgres, Container};

pub fn start_postgres(client: &Cli) -> (Container<Postgres>, String) {
    let container = client.run(Postgres::default());
    let db_str = format!(
        "postgres://postgres:postgres@{}:{}/postgres",
        std::env::var("TESTCONTAINERS_HOST").unwrap_or("localhost".to_string()),
        container.get_host_port_ipv4(5432),
    );
    (container, db_str)
}

pub fn read_stdout_until_contains(
    bufread: &mut BufReader<ChildStdout>,
    pat: &str,
    timeout: u64,
) -> Option<()> {
    let start = Instant::now();
    let mut buf = String::new();
    while let Ok(n) = bufread.read_line(&mut buf) {
        let elapsed = Instant::now() - start;
        if elapsed > Duration::from_secs(timeout) {
            return None;
        }
        if n > 0 {
            if buf.contains(pat) {
                return Some(());
            }
            buf.clear();
        }
    }
    None
}

pub fn send_sigint(pid: u32) {
    Command::new("kill")
        .args(["-SIGINT", &pid.to_string()])
        .spawn()
        .unwrap();
}
